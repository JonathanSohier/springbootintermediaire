package com.fr.sdv.Intermediaire;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fr.sdv.Intermediaire.Repositories.LivreRepository;
import com.fr.sdv.Intermediaire.model.StructLivre;

@RestController
public class LivreController  {
 
    @Autowired 
    private LivreRepository livreRepo;

    public LivreController() {
        System.out.println("EchoController created");
    }

    @PostMapping("/postLivre")
    public ResponseEntity<StructLivre> setLivre(@RequestBody StructLivre livre) {
        livreRepo.save(livre);

        ResponseEntity<StructLivre> responseEntity =new ResponseEntity<>(livre, HttpStatus.OK);
        return responseEntity;
    }

    @GetMapping("/getLivres")
    public ResponseEntity<List<StructLivre>> getLivres() {
        List<StructLivre> livres = livreRepo.findAll();

        ResponseEntity<List<StructLivre>> responseEntity =new ResponseEntity<>(livres, HttpStatus.OK);
        return responseEntity;
    }

    @GetMapping("/getLivresByAutor/{autor}")
    public ResponseEntity<List<StructLivre>> getLivresByAutor(@PathVariable("autor") String autor) {
        List<StructLivre> livres = livreRepo.findByAutor(autor);

        ResponseEntity<List<StructLivre>> responseEntity =new ResponseEntity<>(livres, HttpStatus.OK);
        return responseEntity;
    }

    @GetMapping("/getLivresByTitle/{title}")
    public ResponseEntity<List<StructLivre>> getLivresByTitle(@PathVariable("title") String title) {
        List<StructLivre> livres = livreRepo.findByTitle(title);

        ResponseEntity<List<StructLivre>> responseEntity =new ResponseEntity<>(livres, HttpStatus.OK);
        return responseEntity;
    }
}