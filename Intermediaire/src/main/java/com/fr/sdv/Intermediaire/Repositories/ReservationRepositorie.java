package com.fr.sdv.Intermediaire.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fr.sdv.Intermediaire.model.StructLivre;
import com.fr.sdv.Intermediaire.model.StructReservation;

@Repository
public interface ReservationRepositorie extends JpaRepository<StructReservation,Long>{
    public List<StructLivre> findByReservationDate(String nomTitle);
}
