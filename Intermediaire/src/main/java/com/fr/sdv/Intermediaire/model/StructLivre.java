package com.fr.sdv.Intermediaire.model;

import java.util.Date;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Livre2")
public class StructLivre {
    @Id
    private int id;

    private String autor ;
    private String title ;
    private Date publicationDate;
    private Integer numberOfCopies;
}
