package com.fr.sdv.Intermediaire.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fr.sdv.Intermediaire.model.StructUtilisateur;

@Repository
public interface UtilisateurRepository extends JpaRepository<StructUtilisateur,Long>{ 
    
}
