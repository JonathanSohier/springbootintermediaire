package com.fr.sdv.Intermediaire.model;

import java.sql.Date;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Reservation")
public class StructReservation {
    @Id
    private int id;

    @ManyToOne
    private StructLivre livre;
    @ManyToOne
    private StructUtilisateur user ;
    private Date reservationDate;
}
