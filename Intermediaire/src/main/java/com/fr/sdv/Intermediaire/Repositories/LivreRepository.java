package com.fr.sdv.Intermediaire.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fr.sdv.Intermediaire.model.StructLivre;

@Repository
public interface LivreRepository extends JpaRepository<StructLivre,Long>{
    public List<StructLivre> findByAutor(String nomAutor);
    public List<StructLivre> findByTitle(String nomTitle);
}

