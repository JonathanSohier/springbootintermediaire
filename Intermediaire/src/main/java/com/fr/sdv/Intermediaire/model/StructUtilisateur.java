package com.fr.sdv.Intermediaire.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "User")
public class StructUtilisateur {
    @Id
    private int id;

    private String name;
    private String email;
    private String password;
    private Integer numberOfReservations;
}
