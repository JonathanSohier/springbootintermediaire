package com.fr.sdv.Intermediaire;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fr.sdv.Intermediaire.Repositories.UtilisateurRepository;
import com.fr.sdv.Intermediaire.model.StructUtilisateur;

@RestController
public class UtilisateurController  {

    @Autowired 
    private UtilisateurRepository userRepo;

    public UtilisateurController() {
        System.out.println("EchoController created");
    }

    @PostMapping("/postUser")
    public ResponseEntity<StructUtilisateur> setUser(@RequestBody StructUtilisateur User) {
        userRepo.save(User);

        ResponseEntity<StructUtilisateur> responseEntity =new ResponseEntity<>(User, HttpStatus.OK);
        return responseEntity;
    }

    @GetMapping("/getUsers")
    public ResponseEntity<List<StructUtilisateur>> getUsers() {
        List<StructUtilisateur> livres = userRepo.findAll();

        ResponseEntity<List<StructUtilisateur>> responseEntity =new ResponseEntity<>(livres, HttpStatus.OK);
        return responseEntity;
    }

    @DeleteMapping("/deleteUser/{id}")
    public ResponseEntity<Long> deleteUserById(@PathVariable("id") Long id) {
        userRepo.deleteById(id);

        ResponseEntity<Long> responseEntity =new ResponseEntity<>(id, HttpStatus.OK);
        return responseEntity;
    }
}